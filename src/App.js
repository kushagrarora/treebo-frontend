import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Content from './components/Content';

function App() {
  return (
    <Router>
<    div className="bg-gray-300">
      <div className="pt-20">
        <Navbar />
      </div>
      <Content />
      <Footer />
    </div>
    </Router>
  );
}

export default App;
