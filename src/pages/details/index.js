import React, { useState, useEffect, useRef } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import Loader from "../../components/Loader";
import RoomSelector from "./RoomSelector";

export default function Details() {
  let { id } = useParams();
  const [hotels, setHotels] = useState([]);
  const hotel = useRef({});

  useEffect(() => {
    let hotelReq = axios.get("/api/hotels");
    let priceReq = axios.get("/api/price");
    let detailsReq = axios.get("/api/policy");

    axios.all([hotelReq, priceReq, detailsReq]).then(
      axios.spread((...responses) => {
        const hotelRes = responses[0].data.data;
        const priceRes = responses[1].data.data;
        const detailsRes = responses[2].data.data;

        for (let i = 0; i < hotelRes.length; i++) {
          hotelRes[i] = { ...hotelRes[i], ...priceRes[i], ...detailsRes };
          if (hotelRes[i].id === Number(id)) hotel.current = hotelRes[i];
        }
        setHotels(hotelRes);
      })
    );
  }, [id]);

  if (hotels.length === 0) {
    return <Loader />;
  }

  return (
    <div className="bg-gray-100 text-gray-800 p-4 rounded-md my-6 shadow-md">
      <h1 className="text-3xl font-bold mb-2">{hotel.current.name}</h1>
      <p className="text-lg font-medium mb-2">
        {hotel.current.locality ? hotel.current.locality + ", " : ""}{" "}
        {hotel.current.city}
      </p>
      <div className="whitespace-no-wrap overflow-x-auto mb-5 md:whitespace-normal md:flex">
        <figure className="rounded inline-block overflow-hidden">
          <img
            src="https://cs-images.treebo.com/Treebo_Amber_International/Oak11.jpg?w=235&h=140&fm=pjpg&fit=crop"
            alt="room 1"
          />
        </figure>
        <figure className="rounded inline-block overflow-hidden px-4">
          <img
            src="https://cs-images.treebo.com/Treebo_Amber_International/DSC08057.jpg?w=235&h=140&fm=pjpg&fit=crop"
            alt="room 1"
          />
        </figure>
        <figure className="rounded inline-block overflow-hidden">
          <img
            src="https://cs-images.treebo.com/Treebo_Amber_International/DSC07950.jpg?w=235&h=140&fm=pjpg&fit=crop"
            alt="room 1"
          />
        </figure>
      </div>
      <div className="flex flex-col md:flex-row">
        <div className="md:w-1/2 flex justify-between flex-shrink-0 mr-4 flex-col md:flex-row">
          <article className="mb-5">
            <h2 className="text-xl text-green-800 font-medium">Essentials</h2>
            <ul>
              {hotel.current.essentials.map((item) => (
                <li className="mb-1 ml-3" key={item}>
                  {item}
                </li>
              ))}
            </ul>
          </article>
          <article className="mb-5">
            <h2 className="text-xl text-green-800 font-medium">Policies</h2>
            <ul>
              {hotel.current.policies.map((item) => (
                <li className="mb-1 ml-3" key={item}>
                  {item}
                </li>
              ))}
            </ul>
          </article>
        </div>
        <RoomSelector price={hotel.current.price} />
      </div>
    </div>
  );
}
