import React, {useRef} from "react";
import PropTypes from "prop-types";

function RoomSelector(props) {
  const { price } = props;

  const selection = useRef(null);

  const changeHandler = (val) => {
    selection.current = val;
  }

  const submitHandler = ev => {
    ev.preventDefault();

    if(selection.current === null) {
      alert("No option selected");
      return;
    }

    const isConfirmed = window.confirm("Confirm booking for amount : ₹" + Number(selection.current).toLocaleString("en-IN") + " ?");
    if(isConfirmed)
      alert("Booking confirmed!");
  }

  return (
    <div className="flex-grow flex-shrink-0 border-green-800 border rounded p-4">
      <p className="font-bold mb-2">Choose your option:</p>
      <form className="capitalize" onSubmit={ev => submitHandler(ev)}>
        {Object.entries(price).map((type) => {
          if (type[1] !== null)
            return (
              <div key={type[0]}>
                <input type="radio" name="type" value={type[1]} onChange={ev => changeHandler(ev.target.value)}/> {type[0]} - ₹
                {type[1].toLocaleString("en-IN")}
              </div>
            );
          return (
            <div className="text-gray-400" key={type[0]}>
              <input type="radio" name="type" disabled /> {type[0]} - Sold Out
            </div>
          );
        })}
        <input
          type="submit"
          value="Confirm Booking"
          className="w-full py-2 mr-3 cursor-pointer mt-2 rounded shadow-xs hover:bg-transparent flex-grow border border-green-700 hover:text-green-700 bg-green-700 text-gray-100 md:flex-grow-0 font-medium"
        />
      </form>
    </div>
  );
}

RoomSelector.propTypes = {
  price: PropTypes.object,
};

export default RoomSelector;
