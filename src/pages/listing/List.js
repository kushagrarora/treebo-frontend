import React from "react";
import PropTypes from "prop-types";
import Card from "./Card";

function List(props) {
  const {
    list,
    search
  } = props;

  return (
    <div className="mb-3 mx-3">
      {
        list
        .filter(item => ((item.name+item.city+item.locality).toLowerCase().includes(search.toLowerCase())))
        .map(item => <Card {...item} key={item.id}/> )
      }
    </div>
  );
}

List.propTypes = {
  list: PropTypes.array,
  search: PropTypes.string
};

export default List;
