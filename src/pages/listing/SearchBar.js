import React, { useRef } from "react";
import PropTypes from "prop-types";

export default function SearchBar(props) {
  const {
    setText
  } = props;

  const search = useRef('');

  const setSearch = (val) => {
    search.current = val;
  }

  const searchHandler = (ev) => {
    ev.preventDefault();
    setText(search.current);
  }

  return (
    <form className="flex mb-5 text-sm mt-2 md:text-base" onSubmit={ev => searchHandler(ev)}>
      <input
        onChange={(ev) => setSearch(ev.target.value)}
        type="text"
        className="p-2 rounded mx-3 text-gray-700 flex-grow shadow-md"
        placeholder="Search by name, city or locality"
      />
      <input
        type="submit"
        value="Search"
        className="px-2 mr-3 rounded shadow-xs bg-transparent flex-grow border border-green-700 text-green-700 hover:bg-green-700 hover:text-gray-100 md:flex-grow-0 font-medium"
      />
    </form>
  );
}

SearchBar.propTypes = {
  text : PropTypes.string,
  setText : PropTypes.func
}