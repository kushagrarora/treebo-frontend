import React from "react";
import { Link } from "react-router-dom";

function Card(props) {
  const { id, name, locality, city, price } = props;

  const getCheapestPrice = () => {
    let min = Number.MAX_SAFE_INTEGER;
    const priceArray = Object.values(price);

    for (let i = 0; i < priceArray.length; i++) {
      if (priceArray[i] !== null && priceArray[i] < min) min = priceArray[i];
    }

    return min;
  };

  const minPrice = getCheapestPrice();

  return (
    <div className="bg-gray-100 text-gray-800 p-4 rounded-md my-6 flex flex-col md:flex-row shadow-md">
      <figure className="mx-auto mb-3 md:mb-0 w-full md:ml-0 md:mr-6 md:w-48">
        <img
          src="https://cs-images.treebo.com/Treebo_Tryst_Laxvas/common/Common21.jpg?w=192&h=115&fit=crop&fm=pjpg&auto=compress"
          loading="lazy"
          alt=""
          className="max-w-full max-h-full w-full rounded"
        />
      </figure>
      <div className="md:flex-grow flex justify-between">
        <div className="md:flex-grow">
          <p className="uppercase text-green-600 text-xs">{city}</p>
          <h2 className="text-lg text-gray-900 text-left mb-3 md:text-2xl">
            {name}
          </h2>
          {locality && (
            <p className="text-gray-600 text-sm text-left md:text-lg">
              <span role="img" aria-label="location">
                📌
              </span>{" "}
              {locality}
            </p>
          )}
        </div>
        <div className="w-32 text-sm text-right">
          {minPrice === Number.MAX_SAFE_INTEGER ? (
            <div className="mb-3">
              <div className="text-xl text-gray-500 font-bold">Sold Out</div>
            </div>
          ) : (
            <div className="mb-3">
              <p className="text-xs uppercase text-gray-600 mb-1">
                starting price
              </p>
              <div className="text-xl text-green-700 font-bold">
                ₹{minPrice.toLocaleString("en-IN")}
              </div>
            </div>
          )}

          <Link 
            to={"/hotel/" + id}
            className="py-2 w-full block text-center rounded shadow-xs bg-transparent border border-green-700 text-green-700 hover:bg-green-700 hover:text-gray-100 md:mb-3 md:flex-grow-0 font-medium">
            <span role="img" aria-label="view">
            🔎
            </span>{" "}
            View Details
          </Link>
        </div>
      </div>
    </div>
  );
}

export default Card;
