import React, { useState, useEffect }  from "react";
import axios from "axios";
import SearchBar from "./SearchBar";
import List from "./List";
import Loader from "../../components/Loader";

export default function Listing() {
  const [hotels, setHotels] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    let hotelReq = axios.get("/api/hotels");
    let priceReq = axios.get("/api/price");

    axios
    .all([hotelReq, priceReq])
    .then(axios.spread((...responses) => {
      const hotelRes = responses[0].data.data
      const priceRes = responses[1].data.data

      for (let i = 0; i < hotelRes.length; i++) {
        hotelRes[i] = {...hotelRes[i], ...priceRes[i]}
      }
      setHotels(hotelRes);
    }))
  }, []);

  return (
    <div>
      <SearchBar setText={setSearch} />
      {hotels.length > 0 ? <List list={hotels} search={search} /> : <Loader />}
    </div>
  );
}
