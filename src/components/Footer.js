import React from "react";

export default function Footer() {
  return (
    <footer className="bg-gray-700 py-8 px-3 text-gray-200 text-center">
      Made by <a href="mailto:kushagrarora.17@gmail.com" className="underline">Kushagr Arora</a>
    </footer>
  );
}
