import React from "react";
import Listing from "../pages/listing";
import { Switch, Route } from "react-router-dom";
import Details from "../pages/details";

export default function Content() {
  return (
    <div className="container px-3 max-w-screen-md mx-auto md:px-0">
      <Switch>
        <Route path="/" exact>
          <Listing />
        </Route>
        <Route path="/hotel/:id" exact children={<Details />} />
      </Switch>
    </div>
  );
}
