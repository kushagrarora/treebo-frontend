import React from 'react';
import loaderImage from "../assets/loader_treebo.gif";

export default function Loader() {
  return (
    <div className="w-full">
      <img src={loaderImage} alt="loading" className="w-24 mx-auto"/>
    </div>
  )
}
